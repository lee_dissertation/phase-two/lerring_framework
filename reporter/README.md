# resource_reporter
This project will system resources every X seconds to the api. This will include 
Metrics such as CPU usage, Memory size and disk space.

## Installation
This application is managed by the project lerring-service and installed by setup.
Use [docker-compose](https://docs.docker.com/compose/) to run the application manually. 
```bash
$ docker-compose up --build -d 
```

## Usage

Project runs on its own 

## Roadmap
.
