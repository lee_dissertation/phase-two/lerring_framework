from lib.resources import Resources
from lib.networking import Networking
import requests
import schedule
import time
import json
import threading

"""
Reports system resource usage with 1 second increments, handles updating recent resources as soon as possible
"""
class Reporter():

	def __init__(self):
		self.resources=Resources()
		self.currResources={}
		network=Networking.getNetwork()
		if network['hostname'] == 'N/A':
			#try another interface
			network=Networking.getNetwork('eth1')
		self.currResources['hostname']=network['hostname'].split('.')[0]
		self.currResources['mac']=network['mac']
		#constantly updates resource information as soon as available
		updateJob = threading.Thread(target=self.update)
		updateJob.start()

	def update(self):
		"""
		Updates the current resource usage
		"""
		while True:
			self.currResources.update(self.resources.getResources())

	def report(self):
		"""
		Reports current resource usage
		"""
		headers={'Content-type': 'application/json'}
		try:
			r = requests.put("http://lerring-tail.lerring.co.uk:8081/resources", data=json.dumps(self.currResources), headers=headers, timeout=2)
			print(str(time.time())+' '+str(r.status_code)+'\n')
		except requests.exceptions.Timeout as e:
			self.reportError('time out')
		except requests.exceptions.TooManyRedirects as e:
			self.reportError('too many redirects')
		except requests.exceptions.RequestException as e:
			self.reportError('request exception')
		except requests.exceptions.SSLError as e:
			self.reportError('ssl error')

	def reportError(self,error,message):
		"""
		Reports errors to the docker logs (setup via STD.out)
		"""
		print(str(time.time())+' '+message)
		print(str(e)+"\n")

	def start(self):
		"""
		Schedules reporting every 1 second
		"""
		schedule.every(1).seconds.do(self.report)
		while 1:
			schedule.run_pending()
			time.sleep(1)

if __name__ == "__main__":
	rep=Reporter()
	rep.start()
