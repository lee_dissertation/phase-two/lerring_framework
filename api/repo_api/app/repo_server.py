import falcon
from repo_api import RepoApi
from lib.api.server import EndPoint, Server

"""
/repo endpoint
"""
class Repo(EndPoint):
	def on_get(self, req, resp):
		"""
		GET request handling querying the repos in the API
		Param:
			repos - reponames to get information for
		Returns:
			JSON object containing all matching subjects
		"""
		resp.status = falcon.HTTP_200
		repoNames=req.get_param_as_list('repos')
		resp.body = EndPoint.jsonResponse(self.api.getSubjects(repoNames))

"""
/status  endpoint
"""
class Status(EndPoint):
	def on_get(self, req, resp):
		"""
		GET request handling querying the status of repos
		Param:
			repos - reponames to get status of
		Returns:
			JSON object containing all matching subjects
		"""
		resp.status = falcon.HTTP_200
		repoNames=req.get_param_as_list('repos')
		resp.body = EndPoint.jsonResponse(self.api.getStatus(repoNames))

"""
/remove endpoint
"""
class Remove(EndPoint):
	def on_get(self, req, resp):
		"""
		GET request handling removing repos
		Param:
			repos - reponames to remover
		Returns:
			JSON object containing removed subjects
		"""
		resp.status = falcon.HTTP_200
		repoNames=req.get_param_as_list('repos',required=True)
		resp.body=EndPoint.jsonResponse(self.api.runCallable('remove',repoNames))

"""
/clone endpoint
"""
class Clone(EndPoint):
	def on_get(self, req, resp):
		"""
		GET request handling cloning a repository
		Param:
			repos - reponame to clone
		Returns:
			JSON object containing the newly cloned  subject
		"""
		resp.status=falcon.HTTP_200
		#Only one can be cloned at a time
		repoName=req.get_param('repo',required=True)
		resp.body=EndPoint.jsonResponse(self.api.runCallable('clone',repoName))

"""
/pull endpoint
"""
class Pull(EndPoint):
	def on_get(self, req, resp):
		"""
		GET request to pull a repo
		Param:
			repos - reponames to pull
		Returns:
			JSON object containing all matching subjects
		"""
		resp.status = falcon.HTTP_200
		repoNames=req.get_param_as_list('repos')
		resp.body = EndPoint.jsonResponse(self.api.runCallable('pull',repoNames))

"""
/repo endpoint
"""
class AppServer(Server):

	def __init__(self):
		super().__init__([])
		api=RepoApi()
		self.app.add_route(r'/remove', Remove(api))
		self.app.add_route(r'/', Repo(api))
		self.app.add_route(r'/status', Status(api))
		self.app.add_route(r'/clone', Clone(api))
		self.app.add_route(r'/pull', Pull(api))

repoServer=AppServer()
app=repoServer.getApp()


#app.req_options.auto_parse_form_urlencoded=True
