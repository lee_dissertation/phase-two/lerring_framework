from enum import Enum

"""
Contains priority of state and the state name
"""
class State(Enum):
	CLONING        = (1, 'cloning')
	CLONE          = (1, 'clone')
	CLONE_FAILED   = (1, 'clone failed')
	CLONE_COMPLETE = (1, 'clone complete')

	PULLING       = (2, 'pulling')
	PULL          = (2, 'pull')
	PULL_FAILED   = (2, 'pull failed')
	PULL_COMPLETE = (2, 'pull complete')

	REMOVING        = (3, 'removing')
	REMOVE          = (3, 'remove')
	REMOVE_FAILED   = (3, 'remove failed')
	REMOVE_COMPLETE = (3, 'remove complete')

