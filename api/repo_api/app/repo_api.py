
from repo_manager import RepoManager
from lib.api.api import Api, Callable, CallableCreate

from interfaces.git_client import GitInterface

"""
Backend for the API handling routing of endpoints to callables
"""
class RepoApi(Api):

	def __init__(self):
		super().__init__(RepoManager())
		self.callables['remove']=Callable(
			mgr=self.mgr,
			state="removing",
			allowedStates=[
				"clone failed",
				"clone complete",
				"pull complete",
				"pull failed",
				"remove failed"
			],
			functionToRun=GitInterface.remove
		)
		self.callables['clone']=CallableCreate(
			mgr=self.mgr,
			state="cloning",
			allowedStates=[
				"clone failed",
				"remove complete",
				"clone pending"
			],
			functionToRun=GitInterface.clone
		)
		self.callables['pull']=Callable(
			mgr=self.mgr,
			state="pulling",
			allowedStates=[
				"clone complete",
				"pull failed",
				"pull complete"
			],
			functionToRun=GitInterface.pull
		)
