import shutil
import pathlib
from lib.api.subject import Subject

from repo_state import State

"""
Handles how a repository looks to the API
"""
class Repository(Subject):

	def __init__(self, dir, name, group):
		super().__init__(dir, name)
		self.group=group
		self.state=State.CLONE_COMPLETE.value[1]

	def asDict(self):
		"""
		Defines the response shape, overridden from Subject and used by callables
		"""
		return {
			'name': self.name,
			'dir': self.dir,
			'group': self.group,
			'state': self.state
		}

	def updateState(self, currRepoNames):
		"""
		compares current repo names to the repo name this system is in, then updates accordingly
		"""
		if self.name in currRepoNames:
			return
		if self.state == State.CLONING.value[1]:
			return
		if self.state == State.CLONE_FAILED.value[1]:
			return
		self.setState(State.REMOVE_COMPLETE.value[1])
