import sys
sys.path.append("..")
from repo_state import State
from lib.api.interfaces.interface import Interface

"""
Handles the Git at the lowest level, this could be further extended, here any repository based system can override the 
GitInterface for custom software other than Git
"""
class GitInterface(Interface):

	@staticmethod
	def clone(repo):
		GitInterface.subProcess(
			repo,
			["git","clone",repo.group+'/'+repo.name,repo.dir+'/'+repo.name],
			State.CLONE.value[1]
		)

	@staticmethod
	def pull(repo):
		GitInterface.subProcess(
			repo,
			["git","-C",repo.dir+'/'+repo.name,"pull"],
			State.PULL.value[1]
		)

	@staticmethod
	def remove(repo):
		GitInterface.subProcess(
			repo,
			 ["chmod","-R","u+w",repo.dir+'/'+repo.name],
			State.REMOVE.value[1]
		)
		GitInterface.subProcess(
			repo,
			["rm","-rf",repo.dir+'/'+repo.name],
			State.REMOVE.value[1]
		)

