
from lib.api.manager import Manager
from repo_subject import Repository
from lib.lerring_conf import LerringConf

from repo_state import State

"""
Manages the subjects in the system
"""
class RepoManager(Manager):

	def __init__(self):
		self.config=LerringConf('/lerring/lerring.conf')
		self.group=self.config.getGitGroup()
		super().__init__(
			self.config.getAppDir(),
			'.git'
		)

	#Overriden (Python doesn't feature type casting)
	def genSubjects(self):
		"""
		Generates repository (subject) objects
		Returns:
			generated repositories
		"""
		return [
			Repository(dir=self.dir,name=name,group=self.group)
			for name in self.findSubjectNames()
		]

	#Implemented
	def create(self,repoName):
		"""
		Handles creation of a new repository via the API
		Param:
			reponame - name of repo to clone
		Returns:
			newly created repo as a subject object
		"""
		for repo in self.subjects:
			if repo.name != repoName:
				continue
			#repo exists
			if repo.state != State.REMOVE_COMPLETE.value[1]:
				return None
			repo.state = State.CLONING.value[1]
			return repo
		repo = Repository(dir=self.dir,name=repoName,group=self.group)
		self.subjects.append(repo)
		return repo
