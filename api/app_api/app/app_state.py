from enum import Enum

"""
Potential states an app can be in, numbered based on priority
"""
class State(Enum):
	DESTROYING       = ( 1, 'destroying' )
	DESTROY          = ( 1, 'destroy' )
	DESTROY_COMPLETE = ( 1, 'destroy complete' )
	DESTROY_FAILED   = ( 1, 'destroy failed' )

	REMOVING        = ( 2, 'removing' )
	REMOVE          = ( 2, 'remove' )
	REMOVE_COMPLETE = ( 2, 'remove complete' )
	REMOVE_FAILED   = ( 2, 'remove failed' )

	BUILDING       = ( 3, 'building' )
	BUILD          = ( 3, 'build' )
	BUILD_COMPLETE = ( 3, 'build complete' )
	BUILD_FAILED   = ( 3, 'build failed' )

	STOPPING      = ( 4, 'stopping' )
	STOP          = ( 4, 'stop' )
	STOP_COMPLETE = ( 4, 'stop complete' )
	STOP_FAILED   = ( 4, 'stop failed' )

	DEPLOYING       = ( 5, 'deploying' )
	DEPLOY          = ( 5, 'deploy' )
	DEPLOY_COMPLETE = ( 5, 'deploy complete' )
	DEPLOY_FAILED   = ( 5, 'deploy failed' )

	EXECUTING        = ( 6, 'executing' )
	EXECUTE          = ( 6, 'execute' )
	EXECUTE_COMPLETE = ( 6, 'execute complete' )
	EXECUTE_FAILED   = ( 6, 'execute failed' )
