import docker
import re

class Client():
	client = docker.DockerClient(base_url='unix://var/run/docker.sock')

	@staticmethod
	def getImgNames():
		"""
		returns a list of image names
		Return:
			list of image names
		"""
		return [ re.findall(r"(.*):",image.tags[0])[0] for image in Client.client.images.list() if image.tags ]

	@staticmethod
	def getContainerNames(showAll=False):
		"""
		returns a list of container names
		Args:
			showAll: show all containers, if false only running are shown
		Return:
			list of container names
		"""
		return [ container.name for container in Client.client.containers.list(all=showAll) ]

	@staticmethod
	def exec(containerName,command):
		"""
		executes a command on a container
		Args:
			containerName: name of container to execute
			command: command to execute
		"""
		container = Client.client.containers.get(containerName)
		container.exec_run(command)

	@staticmethod
	def prune():
		"""
		Prunes all dangling items in docker's system
		Returns:
			removed items
		"""
		response={}
		response['volumes']=Client.client.volumes.prune()
		response['images']=Client.client.images.prune()
		response['networks']=Client.client.networks.prune()
		return response

