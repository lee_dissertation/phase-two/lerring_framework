import sys
sys.path.append("..")

from lib.api.interfaces.interface import Interface
from app_state import State

"""
Interacts with docker-compose through the command line, this isn't a secure method
however security higher up handles bad parties
"""
class Compose(Interface):

	@staticmethod
	def build(app):
		"""
		Runs a bash command to build images
		"""
		Compose.subProcess(
			app,
			[
				"docker-compose",
				"-f",
				app.dir+"/"+app.name+"/docker-compose.yml",
				"build"
			],
			State.BUILD.value[1]
		)

	@staticmethod
	def up(app):
		"""
		Runs a bash command to deploy
		"""
		Compose.subProcess(
			app,
			[
				"docker-compose",
				"-f",
				app.dir+"/"+app.name+"/docker-compose.yml",
				"up",
				"--build",
				"-d"
			],
			State.DEPLOY.value[1]
		)

	@staticmethod
	def down(app):
		"""
		Runs a bash command to stop a service
		"""
		Compose.subProcess(
			app,
			["docker-compose","-f",app.dir+"/"+app.name+"/docker-compose.yml","down"],
			State.STOP.value[1]
		)
