import falcon

from app_api import AppApi
from lib.api.server import EndPoint, Server
from lib.api.middleware import  AuthMiddleware

"""
/build endpoint
"""
class Build(EndPoint):
	def on_get(self, req, resp):
		"""
		GET request to handle building a service
		Param:
			name of service/subject to build
		Returns:
			JSON containing new status of subject
		"""
		resp.status = falcon.HTTP_200
		name = req.get_param_as_list('name',required=True)
		resp.body=EndPoint.jsonResponse(self.api.runCallable('build',name))

"""
/deploy endpoint
"""
class Deploy(EndPoint):
	def on_get(self, req, resp):
		"""
		GET request to handle deploying a service
		Param:
			name of service/subject to deploy
		Returns:
			JSON containing new status of subject
		"""
		resp.status = falcon.HTTP_200
		name = req.get_param_as_list('name',required=True)
		resp.body=EndPoint.jsonResponse(self.api.runCallable('deploy', name))

"""
/stop endpoint
"""
class Stop(EndPoint):
	def on_get(self, req, resp):
		"""
		GET request to handle stopping a deployed service
		Param:
			name - name of service/subject to stop
		Returns:
			JSON containing new status of subject
		"""
		resp.status = falcon.HTTP_200
		name = req.get_param_as_list('name',required=True)
		resp.body=EndPoint.jsonResponse(self.api.runCallable('stop', name))

"""
/exec endpoint
"""
class Exec(EndPoint):
	def on_get(self,req,resp):
		"""
		GET request to execute a command on a container
		Param:
			name - subject name
			container - container name
			command - command to run
		Returns:
			JSON containing new status of app
		"""
		name = req.get_param('name',required=True)
		containerName = req.get_param('container',required=True)
		command = req.get_param('command',required=True)
		resp.status=falcon.HTTP_200
		resp.body=EndPoint.jsonResponse(
			self.api.runCallable(
				'exec',
				name,
				(containerName, command)
			)
		)

"""
/status endpoint
"""
class Status(EndPoint):
	def on_get(self, req, resp):
		"""
		GET request for accessing app's status
		Param:
			name - name of subject to query
		Returns:
			JSON containing all matching apps
		"""
		resp.status = falcon.HTTP_200
		name = req.get_param_as_list('name')
		resp.body=EndPoint.jsonResponse(self.api.getStatus(name))

"""
/apps endpoint
"""
class Apps(EndPoint):
	def on_get(self, req, resp):
		"""
		GET request for accessing all apps information
		Param:
			name - name of subject to query
		Returns:
			JSON containing all matching apps
		"""
		resp.status = falcon.HTTP_200
		name = req.get_param_as_list('name')
		resp.body=EndPoint.jsonResponse(self.api.getApps(name))


"""
Handles the App instance which is used by Gunicorn
"""
class AppServer(Server):

	def __init__(self):
		auth=AuthMiddleware(
			permissions=['apps'],
			exemptRoutes=[]
		)
		super().__init__([auth])
		api=AppApi()
		self.app.add_route(r'/build', Build(api))
		self.app.add_route(r'/deploy', Deploy(api))
		self.app.add_route(r'/stop', Stop(api))
		self.app.add_route(r'/exec', Exec(api))
		self.app.add_route(r'/status', Status(api))
		self.app.add_route(r'/', Apps(api))

appServer=AppServer()
app=appServer.getApp()

