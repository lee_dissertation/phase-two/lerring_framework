import time
import schedule

from interfaces.docker_client import Client
from lib.api.manager import Manager
from app_subject import Application
from lib.lerring_conf import LerringConf

"""
Manager is responsible for subjects
"""
class AppManager(Manager):

	def __init__(self):
		self.config=LerringConf('/lerring/lerring.conf')
		super().__init__(
			self.config.getAppDir(),
			'docker-compose.yml'
		)

	#Overriden (Python doesn't feature type casting)
	def genSubjects(self):
		"""
		Generates application instances for every subject found
		Return:
			array of application instances
		"""
		return [
			Application(dir=self.dir,name=name)
			for name in self.findSubjectNames()
		]

	#Overriden
	def scheduleUpdate(self):
		"""
		Schedules the tasks of pruning and updating state
		"""
		schedule.every(5).seconds.do(self.updateState)
		schedule.every(2).minutes.do(Client.prune)
		while 1:
			schedule.run_pending()
			time.sleep(5)

	#Overridden ( Subject state is determined by currentImages and all/running containers )
	def updateState(self):
		"""
		Gets current knowledge of the docker subsystem
		allows subjects to update their state
		"""
		self.updateSubjects()
		currImageNames=Client.getImgNames()
		allContainers=Client.getContainerNames()
		runningContainers=Client.getContainerNames(showAll=True)
		for app in self.subjects:
			app.updateState(currImageNames,allContainers,runningContainers)

