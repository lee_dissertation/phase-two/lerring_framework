import yaml
from app_state import State
from lib.api.subject import Subject

"""
Subject to represent an Application on the API side
"""
class Application(Subject):

	def __init__(self,dir,name):
		super().__init__(dir, name)
		self.services=[]
		self.readComposeFile()
		self.state=State.DESTROY_COMPLETE.value[1]

	def asDict(self):
		"""
		Used for the /apps endpoint to show what a subject looks like
		"""
		return {
			'name': self.name,
			'services': self.services
		}

	#overridden
	def updateState(self,currImageNames,allContainers,runningContainers):
		"""
		Updates the state of the subject by considering all services and determining which is the highest priority
		"""
		#if status is failed, don't overwrite
		if self.state.endswith('failed'):
			return

		for service in self.services:
			self.updateServiceStatus(
				service,
				currImageNames,
				allContainers,
				runningContainers
			)
		#set state based on priority of services
		self.setState( sorted([ service['status'] for service in self.services ])[0] )

	def updateServiceStatus(self,service,currImageNames,allContainers,runningContainers):
		"""
		Updates the service's status
		"""
		if service['image'] not in currImageNames:
			if service['status'] != State.BUILDING.value[1] and service['status'] != State.DEPLOYING.value[1]:
				service['status']=State.DESTROY_COMPLETE.value[1]
				return

		if 'container_name' not in service or service['container_name'] not in allContainers:
			if service['status'] != State.DEPLOYING.value[1]:
				service['status']=State.BUILD_COMPLETE.value[1]
				return

		if service['container_name'] not in runningContainers:
			if service['status'] != State.DEPLOYING.value[1]:
				service['status']=State.STOP_COMPLETE.value[1]
				return

		if service['status'] != State.STOPPING.value[1]:
			service['status']=State.DEPLOY_COMPLETE.value[1]

	@staticmethod
	def findOne(data, key):
		"""
		Searchest a dict for a key
		"""
		if key in data:
			return data[key]
		for k, v in data.items():
			if isinstance(v, dict):
				found=Application.find(v, key)
				if found:
					return found
		return None

	@staticmethod
	def findAll(data, key, list):
		"""
		Searches a dict for all instances of a matching key
		"""
		if key in data:
			list.append(data[key])
		for k, v in data.items():
			if isinstance(v, dict):
				list=Application.find(v, key, list)
		return list

	def readComposeFile(self):
		"""
		Reads the docker compose file to populate services
		"""
		with open(self.dir+'/'+self.name+"/docker-compose.yml") as ymlFile:
			conf=yaml.safe_load(ymlFile)
			services=Application.findOne(conf,'services')

			for name,service in services.items():
				service['name']=name
				self.services.append(service)

			for service in self.services:
				service['status']=State.DESTROY_COMPLETE.value[1]
