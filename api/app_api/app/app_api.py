
from interfaces.docker_compose import Compose
from interfaces.docker_client import Client

from app_state import State

from app_manager import AppManager
from lib.api.api import Api, Callable

import threading

"""
Backend for an API, uses callables to route requests
"""
class AppApi(Api):

	def __init__(self):
		super().__init__(AppManager())
		self.callables['build']=Callable(
			mgr=self.mgr,
			state=State.BUILDING.value[1],
			allowedStates=[
				State.BUILD_FAILED.value[1],
				State.BUILD_COMPLETE.value[1],
				State.DESTROY_COMPLETE.value[1],
				State.DEPLOY_FAILED.value[1],
				State.STOP_COMPLETE.value[1]
			],
			functionToRun=Compose.build
		)
		self.callables['deploy']=Callable(
			mgr=self.mgr,
			state=State.DEPLOYING.value[1],
			allowedStates=[
				State.DESTROY_COMPLETE.value[1],
				State.BUILD_COMPLETE.value[1],
				State.DEPLOY_COMPLETE.value[1],
				State.DEPLOY_FAILED.value[1]
			],
			functionToRun=Compose.up
		)
		self.callables['exec']=CallableExec(
			mgr=self.mgr,
			state=State.EXECUTING.value[1],
			allowedStates=[
				State.DEPLOY_COMPLETE.value[1],
				State.EXECUTE_COMPLETE.value[1],
				State.EXECUTE_FAILED.value[1]
			],
			functionToRun=Client.exec
		)
		self.callables['stop']=Callable(
			mgr=self.mgr,
			state=State.STOPPING.value[1],
			allowedStates=[
				State.DEPLOY_COMPLETE.value[1],
				State.EXECUTE_COMPLETE.value[1],
				State.EXECUTE_FAILED.value[1]
			],
			functionToRun=Compose.down
		)

	def getApps(self,names):
		"""
		Gets subjects in the form of a dictionary
		Param:
			names - names to query
		Returns:
			python dictionary with matching results
		"""
		return self.mgr.getSubjectsAsDict(names)

"""
Custom Callable for handling the exec endpoint
"""
class CallableExec(Callable):


	def __init__(self,mgr,state,allowedStates,functionToRun):
		super().__init__(mgr,state,allowedStates,functionToRun)


	def runAll(self, name, param):
		"""
		Overridden method to handle searching based on container name
		"""
		containerName=param[0]
		command=param[1]
		response={}
		subjects = self.mgr.getSubjects(name)
		if not subjects:
			return { 'error' : 'subject not found!' }
		for subject in subjects:
			response[subject.name]=self.run(subject,containerName,command)
		return response

	def run(self, subject, containerName, command):
		"""
		Handles searching for container name instead of subject name
		"""
		for service in subject.services:
			if 'container_name' in service and service['container_name'] == containerName:
				service['status']=self.state
				t=threading.Thread(target=self.functionToRun,args=(containerName,command))
				t.start()
				return self.state
		return { 'error' : 'could not find '+containerName }
