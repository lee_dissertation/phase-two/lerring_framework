# application_manager
This project acts as a wrapper for docker-compose, producing restful apis which are able to be accessed by a 
third party.
## Installation
This application is managed by the project lerring-service and installed by setup. Use 
[docker-compose](https://docs.docker.com/compose/) to run the application manually. 
```
docker-compose up --build -d 
```
## Usage
Swagger shall be included at a later date
## Roadmap
