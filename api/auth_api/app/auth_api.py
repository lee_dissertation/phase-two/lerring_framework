import requests

from lib.jwt_helpers import *

from lib.lerring_conf import LerringConf
from lib.networking import Networking

import json
import time

"""
Backend for authentication instance
"""
class AuthApi():

	def __init__(self):
		conf=LerringConf('/lerring/lerring.conf')
		self.privKeyPem=conf.getPrivSignature()
		#handles jwts using the RS256 alg
		self.rsJwtHandler=SelfSignedJwtRS256(
			conf.getPubSignature(),
			self.privKeyPem
		)
		#handles jwts using the HS256 alg
		self.hsJwtHandler=SelfSignedJwtHS256(
			conf.getUuid()
		)


	def getJwt(self, requestToken, mac):
		"""
		Verifies the token using the public key from the database
		associated with the mac returns a jwt providing permission
		to access set resources if recognised as head node

		Param:
			input token
			mac address associated with token
		Return:
			generated jwt signed by node
		Exceptions:
			failure parsing or generating token
		"""
		try:
			self.verifyToken(requestToken, mac)
			responseToken=self.responseToken(
				mac,
				self.isHead(
					self.getIp(mac)
				)
			)
			return 	{ 'jwt' : responseToken	}
		except Exception as e:
			return {
				'error' : 'failed validating token',
				'message' : str(e)
			}

	def verifyToken(self, requestToken, mac):
		"""
		Gets the public key corresponding to its mac address,
		if the public key verifies the token no exception is raised
		if the public key doesn't verify the token,
		a suitable exception is raised by the validateToken method

		Param:
			request token to verify
			mac to get public key for
		Return:
			payload
		Exceptions:
			exceptions when failing to verify signature
		"""
		results=requests.get("http://lerring-tail:8081/keys?mac="+mac).json()
		if len(results) == 0:
			raise Exception("Could not aquire pem for "+mac)

		for result in results:
			if result['mac'] == mac:
				pem=result['pem']

		alg=Jwt.getHeaders(requestToken)['alg']
		if alg == "HS256":
			return self.hsJwtHandler.validateToken(requestToken)
		if alg == "RS256":
			return self.rsJwtHandler.validateToken(requestToken)

		raise Exception("incorrect algorithm used for jwt")

	def getIp(self, mac):
		"""
		Gets the ip associated with mac address from the dns server
		Param:
			mac address to get ip for
		Returns:
			ip if found else none
		"""
		for lease in requests.get('http://lerring-router:5010/leases').json():
			if lease['mac'] == mac:
				return lease['ip']
		return None

	def isHead(self, reqIp):
		"""
		Checks the dns server for entries including head
		"""
		for ip, hostnames in dict(requests.get('http://lerring-router:5010/hosts').json()).items():
			if 'lerring-head' in hostnames and ip == reqIp:
				return True
		return False

	def responseToken(self, mac, isHead):
		"""
		Generates a response token
		Param:
			mac, used for audience claim
			isHead, used to provide permissions
		Return:
			generated JWT, signed with the hosts private key
		"""
		headers={
			'alg':'RS256',
			'typ':'JWT'
		}
		iat=int(time.time())
		payload={
			'sub': Networking.getMac(),
			'aud': mac,
			'permissions': ['apps','repos'] if isHead else [],
			'iat': iat,
			'exp': iat+3600
		}
		return Jwt.createToken(
			payload=payload,
			secret=self.privKeyPem,
			headers=headers
		)
