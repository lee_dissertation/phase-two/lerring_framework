import falcon
from lib.api.server import EndPoint, Server
from auth_api import AuthApi
from lib.api.middleware import  AuthMiddleware

"""
/auth endpoint
"""
class Auth(EndPoint):
	def on_get(self, req, resp):
		"""
		Expects:
			authorization header containing a jwt signed by the sender
			mac address associated with the signed jwt
		"""
		resp.status = falcon.HTTP_200
		token=req.get_header('Authorization',required=True)
		id=req.get_header('Mac-Addr',required=True)
		resp.body = EndPoint.jsonResponse(self.api.getJwt(token,id))

"""
/test endpoint
"""
class Test(EndPoint):
	def on_get(self, req, resp):
		"""
		Used for testing to validate a token works with the AuthMiddleware
		"""
		resp.status=falcon.HTTP_200
		resp.body=EndPoint.jsonResponse({'message':'Authentication success!'})

"""
Auth server is responsible for providing authorization based on a senders
role in the system, the authorization enables the sender to access the apps and repos api
"""
class AuthServer(Server):

	def __init__(self):
		auth=AuthMiddleware(
			permissions=['apps','repos'],
			exemptRoutes=['/']
		)
		super().__init__([auth])
		api=AuthApi()
		self.app.add_route(r'/', Auth(api))
		self.app.add_route(r'/test', Test(api))

authServer=AuthServer()
app=authServer.getApp()
