import requests
import json
import time
from threading import Thread
from ping3 import ping
from base_nodes import *
from elector import Elector

"""
Corresponds to a router node
"""
class LerringRouter(NetworkingNode):

	def __init__(self, uuid):
		super().__init__(['router_framework'],uuid,'eth1')

	def start(self):
		"""
		This waits for DNS after starting then retrieves networking details once available
		"""
		super().start()
		self.waitForDns()
		self.networkDetails=Networking.getNetwork(self.interface)

"""
Corresponds to a worker node
"""
class LerringWorker(MessagingNode):

	def __init__(self, uuid, broker):
		self.isHead = False
		self.elector = None
		self.status = None
		subscriptions = {
			'head/election/status':self.electionStatus,
			'head/election/candidate':self.addCandidate
		}
		super().__init__([],uuid, broker, subscriptions)

	def addCandidate(self,candidateIp):
		"""
		Adds a candidate to the elector
		Param:
			candidateIp - identifier of the candidate
		"""
		if self.elector:
			self.elector.addCandidate(candidateIp)

	def electionStatus(self, status):
		"""
		Routes the logic of the elector based on the status
		Param:
			status - new election status
		"""
		#if status hasn't changed
		if status == self.status:
			return
		self.status=status
		#nominate an existing candidate, or self
		#resets the elector
		if status == "nominating":
			print("Nominating!")
			print("")
			vote = self.elector.getElected()[0] if self.elector else self.networkDetails['ip']
			self.elector = Elector()
			self.client.publish(
				'head/election/candidate',
				vote
			)
			return
		#checks for electees
		#re run election if joint winners exist
		if status == "electing" and self.elector:
			print("Electing!")
			elected = self.elector.getElected()
			print("Elected: "+str(elected))
			print("")
			if len(elected) > 1:
				self.electionStart()
			elif self.networkDetails['ip'] == elected[0]:
				# "I have won!"
				self.electionVictory()
			return
		if status == "finished":
			print("Finished!")
			print("")
			self.elector = None

	def electionStart(self):
		"""
		Stats the election while checking for messages which indicate one exists
		"""
		#concurrency check for nomination already initiated
		if self.status == "nominating":
			return
		print("Starting election!")
		print("")
		self.isHead = False
		self.client.publish('head/election/status', 'nominating')
		time.sleep(5)
		#concurrency check for election already initiated
		if self.status != "nominating":
			return
		self.client.publish('head/election/status', 'electing')

	def electionVictory(self):
		"""
		Runs in the case of an election victory,
		updates the DNS to point head node towards this
		"""
		print("Winner!")
		self.isHead = True
		data = json.dumps([{
			"ip": self.networkDetails['ip'],
			"hostname": "lerring-head"
		}])
		requests.put(
			url="http://lerring-router:5010/hosts",
			data=data,
			headers={'Content-Type': 'application/json'}
		)
		#give time for updating records
		time.sleep(5)
		self.client.publish('head/election/status', 'finished',retain=True)

	def heartbeat(self):
		"""
		Runs in an inidivdual thread to ping the head node every 5 seconds
		Upon failing to do so an election is started if one isn't alread running
		"""
		while 1:
			if not ping('lerring-head.lerring.co.uk', timeout=5) and not self.isHead:
				if not self.status or self.status == "finished":
					print("Can't find head!!")
					self.electionStart()
			time.sleep(5)

	def start(self):
		"""
		Waits for the DNS, then begins pinging the head node
		"""
		self.waitForDns()
		#giving time to recieve retained messages
		time.sleep(2)
		heartbeat = Thread(target=self.heartbeat)
		heartbeat.start()

"""
Corresponds to a Tail node
"""
class LerringTail(MessagingNode):

	def __init__(self, uuid, broker):
		subscriptions={}
		super().__init__(['tail_framework'],uuid, broker, subscriptions)

	def start(self):
		"""
		Once networking is prepared, update the DNS entry for tail node
		"""
		self.waitForDns()
		data = json.dumps([{
			"ip": self.networkDetails['ip'],
			"hostname": "lerring-tail"
		}])
		requests.put(
			url="http://lerring-router:5010/hosts",
			data=data,
			headers = {'Content-Type': 'application/json'}
		)
		super().start()
