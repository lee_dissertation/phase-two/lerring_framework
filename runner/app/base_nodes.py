import requests
from lib.jwt_helpers import *

"""
Node class handles the base functionaltiy all nodes relate to
"""
class Node():

	def __init__(self,apps,uuid):
		self.apps=['resource_reporter']+apps
		self.uuid=uuid

	def start(self):
		"""
		Deploys the apps, is run by the derived classes once they have populated self.apps
		"""
		print("Deploying Apps!")
		token=SelfSignedJwtHS256(
			self.uuid
		).createToken(payload={'permissions':['apps']},headers={'typ':'JWT'})
		print("Token: "+token,flush=True)
		print(requests.get(
			url="http://localhost:8080/apps/deploy",
			params={'name':self.apps},
			headers={'Authorization':token}
		).content,flush=True)


from lib.networking import Networking
from lib.keys import KeyFactory, KeyPair
import json
import time

"""
A Networking node is a node which has access to the cloud via ethernet
"""
class NetworkingNode(Node):

	def __init__(self, apps, uuid, interface='eth0'):
		super().__init__(apps, uuid)
		self.interface=interface

	@staticmethod
	def wait(url,errorMessage):
		"""
		Used to allow the system to wait untill a url responds
		"""
		while True:
			try:
				resp = requests.get(
					url
				)
				resp.raise_for_status()
				if resp.status_code == 200:
					return
			except Exception as e:
				pass
			print(errorMessage)
			time.sleep(5)

	@staticmethod
	def waitForKeys():
		"""
		Used to allow the system to wait untill the tail node has setup the keys database
		"""
		NetworkingNode.wait("http://lerring-tail:8081/keys","Could not connect to keys db! sleeping...")

	@staticmethod
	def waitForDns():
		"""
		Used to allow the system to wait untill the router has setup the dns
		"""
		NetworkingNode.wait("http://lerring-router:5010", "Could not connect to dns! sleeping..")

	def loadKeys(self):
		"""
		Generates keys, writes them as pem to the file system
		and sends to the database
		"""
		#keep network details updated incase dns has changed anything
		self.networkDetails=Networking.getNetwork(self.interface)
		#wait for key db
		NetworkingNode.waitForKeys()
		#gen keys
		keyPair=KeyFactory.genRsaKey()
		#write keys
		dir='/lerring/signature_keys'
		keyPair.writeKeys(
			dir+'/pub.pem',
			dir+'/priv.pem'
		)
		#send to database
		self.pubKeyPem=KeyPair.publicAsPem(keyPair.publicKey).decode('utf-8')
		data=json.dumps({
			'mac':self.networkDetails['mac'],
			'pem':self.pubKeyPem
		})
		print(requests.put(
			url="http://lerring-tail:8081/keys",
			data=data,
			headers = {'Content-Type': 'application/json'}
		).content)

import threading
import paho.mqtt.client as mqtt
from ping3 import ping

"""
A MessagingNode extends the NetworkingNode to handle the MQTT broker
"""
class MessagingNode(NetworkingNode):

	def __init__(self, apps, uuid, broker, subscriptions, interface='eth0'):
		super().__init__(apps, uuid, interface)
		self.uuid=uuid
		self.broker=broker
		self.networkDetails=Networking.getNetwork(self.interface)
		self.client=mqtt.Client(self.networkDetails['hostname'],clean_session=True)
		self.client.connect(self.broker)
		self.client.loop_start()
		self.client.on_message=self.onMessage
		self.subscriptions=subscriptions
		self.subscribe()

	def subscribe(self):
		"""
		Subscribes to defined topics
		"""
		for topic in self.subscriptions.keys():
			self.client.subscribe(topic)

	def onMessage(self, client, userdata, message):
		"""
		Handles an mqtt message in a seperate thread
		Params:
			client   sent client
			userdata data from user
			message  containing message
		"""
		payload=str(message.payload.decode("utf-8"))
		topic=message.topic
		t=threading.Thread(target=self.handleMessage,args=(topic,payload,))
		t.start()

	def handleMessage(self,topic,payload):
		"""
		Message handler, routing done using subscriptions
		Params:
			topic subject of message
			payload data sent to topic
		"""
		#print("")
		#print("Topic: "+topic)
		#print("Message: "+payload)
		self.subscriptions.get(topic)(payload)
