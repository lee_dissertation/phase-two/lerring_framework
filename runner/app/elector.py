"""
This class is used to hold an election process in memory
Candidates are stored with the structure of  { 'ip' : voteCount }
"""
class Elector():

	def __init__(self):
		"""
		Initialising an elector corresponds to starting a new election
		"""
		self.candidates = {}

	def addCandidate(self, ip):
		"""
		This handles incrementing a vote if it exists
		Params:
			ip - ip address of candidate
		"""
		try:
			self.candidates[ip] = self.candidates[ip] + 1
		except KeyError:
			self.candidates[ip] = 1

	def getElected(self):
		"""
		Returns the candidates in order of most votes
		"""
		sort = sorted(
		    self.candidates.items(),
		    key=lambda vote: vote[1],
		    reverse=True
		)
		return sorted([ip for ip, votes in sort if votes == sort[0][1]])
