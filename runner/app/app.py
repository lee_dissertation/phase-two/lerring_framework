import time
import os
import requests
import sys
from lerring_nodes import *
from lib.networking import Networking
from lib.lerring_conf import LerringConf


"""
Main logic for the runner, handles the acitivty diagram
"""
class Main():

	conf=LerringConf('/lerring/lerring.conf')

	def __init__(self):
		"""
		Main method to setup and run the application
		"""
		self.node=self.determineNode()
		self.node.start()
		self.node.waitForKeys()
		self.node.loadKeys()

	def determineNode(self):
		"""
		Determines node type
		"""
		nodeType=Main.conf.getType()
		uuid=Main.conf.getUuid()
		broker=Main.conf.getBroker()
		print("NODE="+nodeType)

		if nodeType == 'ROUTER':
			return LerringRouter(uuid)

		if nodeType == 'TAIL':
			return LerringTail(uuid, broker)

		if nodeType == 'LERRING':
			return LerringWorker(uuid, broker)

		if nodeType == 'TEST':
			return NetworkingNode([], uuid)

		raise Exception("Unable to determine node type!")

if __name__ == "__main__":
	#give system time to setup apis
	time.sleep(10)
	main = Main()
